# Update firmware for collecting IMU data with higher ODR
The default Nicla Sense ME only has an ODR of 400 Hz. To increase the sampling rate of the IMU, update the firmware of the BHI260AP with the binary "NiclaSenseME-nobsx-1600hz-IMU-passthrough-flash.fw" from the pullrequest #68 in the GitHub repository of the Nicla Sense ME firmware: https://github.com/arduino/nicla-sense-me-fw
* https://github.com/bstbud/nicla-sense-me-fw/blob/exp_imu_data_col_high_odr/Arduino_BHY2/examples/SyncAndCollectIMUData/README.md

# How to update the BHI260AP firmware

Use the sketch update_fw.ino to flash a binary (fw.h) that is used to update the on-flash firmware for BHI260AP on Nicla Sense ME.
Steps:
 1. Generate the "fw.h" desired (see steps below) and put it in the same folder as this sketch
 2. Upload (program) this sketch to the Nicla Sense Board and immediately open the Serial Monitor from the Arduino IDE
 3. Check the messages in the Serial Monitor and wait until "BHY FW Upload Done!" shows up
 4. Upload another sketch such as "Examples->Arduino_BHY2->Standalone" to the Nicla Sense Board and you are all set

## Steps to generate the desired "fw.h"
*  To use a different BHI260 FW binary, use the command below to generate the "fw.h"
*  note: the FW binary should have the name pattern: xxx-flash.fw or xxx_flash.fw
```sh
 $ mv xxx-flash.fw BHI260AP_NiclaSenseME-flash.fw 
 $ echo const > fw.h && xxd -i BHI260AP_NiclaSenseME-flash.fw >> fw.h
```
This tutorial has been taken from:
* https://github.com/arduino/nicla-sense-me-fw/tree/main/Arduino_BHY2/examples/BHYFirmwareUpdate