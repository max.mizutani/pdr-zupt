#include "Arduino_BHY2.h"

SensorXYZ mag(SENSOR_ID_MAG);
SensorXYZ accel(SENSOR_ID_ACC_PASS);
SensorXYZ gyro(SENSOR_ID_GYRO_PASS);
SensorOrientation ori(SENSOR_ID_ORI);

void setup() {
  Serial.begin(115200);
  BHY2.begin();
  
  mag.begin();  
  accel.begin();
  gyro.begin();
  ori.begin();
}

void loop() {
  static auto lastCheck = millis();
  BHY2.update();

  if (millis() - lastCheck >= 5) {
    lastCheck = millis();
    Serial.println("Millis: "+String(lastCheck)+" X_acc: "+String(accel.x())+" Y_acc: "+String(accel.y())+" Z_acc: "+String(accel.z())+
    " X_gyr: "+String(gyro.x())+" Y_gyr: "+String(gyro.y())+" Z_gyr: "+String(gyro.z())+
    " X_mag: "+String(mag.x())+" Y_mag: "+String(mag.y())+" Z_mag: "+String(mag.z())+
    " roll: "+String(ori.roll())+" pitch: "+String(ori.pitch())+" yaw: "+String(ori.heading()));
  }
}
