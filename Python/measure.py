import serial

serial_port = '/dev/ttyACM0'  # Use /dev/ttyACM0 for Linux
baud_rate = 115200;  # Set the Baud rate from Serial.begin() in Arduino IDE
write_to_file_path = "../Measurements/Series_3/100m_03.txt"

output_file = open(write_to_file_path, "w+")
ser = serial.Serial(serial_port, baud_rate)

try:
    i = 0
    while True:
        if i < 10: # Discard first 10 records to avoid mixed up lines
            line = ser.readline()
            i += 1
            continue
        line = ser.readline()
        line = line.decode("utf-8")      # ser.readline returns a binary, convert to string
        output_file.write(line[:-1])     # Add [:-1] to cut off the \n
except KeyboardInterrupt:
    ser.close()
    output_file.close()