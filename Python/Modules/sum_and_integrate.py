import numpy as np
from Modules.calibrate import calibrate
from Modules.trafos import transform, board_to_ref
from Modules.integrate import integrate_wrap, world_cord, board_cord

def sum_and_integrate(df):
    
    t_diff = df["sec"] - df["sec"].shift()

    df["x_r"] = 0
    df["x_p"] = 0
    df["x_y"] = 0

    # df["a_n"] = 0
    # df["a_e"] = 0
    # df["a_u"] = 0

    df["x_n"] = 0
    df["x_e"] = 0
    df["x_u"] = 0

    standing = False
    for index in range(1, len(df)):
        if df.loc[index, 'step']:
            df.loc[index, ['v_n', 'v_e', 'v_u']] = 0,0,0
            df.loc[index, "a_n"], df.loc[index, "a_e"], df.loc[index, "a_u"] = board_to_ref(
                df.loc[index, "a_1"],
                df.loc[index, "a_2"],
                df.loc[index, "a_3"],
                df.loc[index, "x_r"],
                df.loc[index, "x_p"],
                df.loc[index, "x_y"],
            ),
            if not standing:
                start = index
            standing = True
        elif standing:
            end = index
            stat_data = df.loc[start:end, ["a_1", "a_2", "a_3", "m_1", "m_2", "m_3"]].mean()
            df.loc[start:end,["x_r", "x_p", "x_y"]] = calibrate(stat_data)
            df.loc[index, "a_n"], df.loc[index, "a_e"], df.loc[index, "a_u"] = board_to_ref(
                df.loc[index, "a_1"],
                df.loc[index, "a_2"],
                df.loc[index, "a_3"],
                df.loc[index, "x_r"],
                df.loc[index, "x_p"],
                df.loc[index, "x_y"],
            ),
            standing = False
        else:
            df.loc[index, "a_n"], df.loc[index, "a_e"], df.loc[index, "a_u"] = board_to_ref(
                df.loc[index, "a_1"],
                df.loc[index, "a_2"],
                df.loc[index, "a_3"],
                df.loc[index, "x_r"],
                df.loc[index, "x_p"],
                df.loc[index, "x_y"],
            ),
            df.loc[index, ['x_r', 'x_p', 'x_y']] = integrate_wrap(df, 'v', board_cord, index)

    # for index in range(r_limit + 1, len(df)):
    #     df.loc[index, ['x_r', 'x_p', 'x_y']] = integrate_wrap(df, 'v', board_cord, index)

    # transform(df)

    df["v_n"] = 0
    df["v_e"] = 0
    df["v_u"] = 0

    for index in range(1, len(df)):
        if not (df.loc[index, 'step']):
            df.loc[index, ['v_n', 'v_e', 'v_u']] = integrate_wrap(df, 'a', world_cord, index)
        else:
            df.loc[index, ['v_n', 'v_e', 'v_u']] = 0,0,0

    df["x_n"] = 0
    df["x_e"] = 0
    df["x_u"] = 0

    for index in range(1, len(df)):
        df.loc[index, ['x_n', 'x_e', 'x_u']] = integrate_wrap(df, 'v', world_cord, index)

    # def pythagoras(x, y):
    #     return np.sqrt(pow(x, 2) + pow(y, 2))

    # df["d_h"] = df.apply(lambda row: pythagoras(row["d_n"], row["d_e"]), axis=1)

    # df["x_h"] = df["d_h"].cumsum()

    # # Fill the first row with 0, as there is no previous row to calculate the difference
    # df.loc[0, ["d_n", "d_e", "d_u", "d_h", "x_n", "x_e", "x_u", "x_h"]] = 0

    return df

#ahrs