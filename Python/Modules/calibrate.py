from Modules.trafos import orientate_board

def calibrate(stat_data):
    orientation = orientate_board(stat_data)
    return orientation