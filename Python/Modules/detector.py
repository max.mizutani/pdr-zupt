import numpy as np
import pandas

def detector(df, time_since_step):

    threshold = 30 *2*np.pi/360
    cooldown = 0

    if (time_since_step < cooldown):
        return False

    step = df.apply(
            lambda row: bound_check(
                row["v_r"],
                row["v_p"],
                row["v_y"],
                threshold,
            ),
            axis=1,
        )
    return step

def bound_check(a, b, c, threshold):
    return abs(a) < threshold and abs(b) < threshold and abs(c) < threshold
    

def step_filter(df, column_name, block_width):

    if column_name not in df.columns:
        raise ValueError(f"Column '{column_name}' not found in the DataFrame.")

    result_column = f"{column_name}_filtered"
    df[result_column] = df[column_name]

    for index, row in df.iterrows():
        if row[column_name]:
            start_index = max(0, index - block_width)
            end_index = min(len(df), index + block_width)
            block_sum = df.iloc[start_index:end_index][column_name].sum()

            if block_sum < block_width*1.1: # parameter to set bufferzone
                df.at[index, result_column] = False

    df.drop(column_name, axis=1, inplace=True)
    df.rename(columns={result_column: column_name}, inplace=True)
