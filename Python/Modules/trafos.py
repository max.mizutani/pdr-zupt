

import numpy as np


def orientate_board(stationary_a_and_m):

    a_1, a_2, a_3, m_1, m_2, m_3 = stationary_a_and_m

    r = np.arctan(a_2/a_3)
    p = np.arctan(-a_1/np.sqrt(pow(a_2,2) + pow(a_3,2)))
    y = np.arctan((m_3 * np.sin(r) - m_2 * np.cos(r))/(m_1 * np.cos(p) + m_2 * np.sin(r) + m_3 * np.sin(p) * np.cos(r)))

    return r, p, y

def board_to_ref(a_1, a_2, a_3, r, p, y):

    a_north = a_1*np.cos(p)*np.cos(y) + a_2*(np.sin(r)*np.sin(p)*np.cos(y)-np.cos(r)*np.sin(y)) + a_3*(np.cos(r)*np.sin(p)*np.cos(y)+np.sin(r)*np.sin(y))
    a_east  = a_1*np.cos(p)*np.sin(y) + a_2*(np.sin(r)*np.sin(p)*np.sin(y)+np.cos(r)*np.cos(y)) + a_3*(np.cos(r)*np.sin(p)*np.sin(y)-np.sin(r)*np.cos(y))
    a_up    = -a_1*np.sin(p) + a_2*np.sin(r)*np.cos(p) + a_3*np.cos(r)*np.cos(p)

    # account for earth gravitation:
    # a_up -= np.sqrt(pow(a_north,2) + pow(a_east,2) + pow(a_up,2))
    a_up -= 4096

    return acc_to_metric(a_north), acc_to_metric(a_east), acc_to_metric(a_up)

def board_to_ref_quart(a_1, a_2, a_3, q_x, q_y, q_z, q_w):

    a_north = a_1 * (pow(q_x, 2) + pow(q_y, 2) - pow(q_z, 2) - pow(q_w, 2)) + a_2*(2*q_y*q_z - 2*q_x*q_w) + a_3*(2*q_y*q_w+2*q_x*q_z)
    a_east  = a_1 * (2*q_y*q_z + 2*q_x*q_w) + a_2*(pow(q_x, 2) - pow(q_y, 2) + pow(q_z, 2) - pow(q_w, 2)) + a_3*(2*q_z*q_w - 2*q_x*q_y)
    a_up    = a_1*(2*q_y*q_w - 2*q_x*q_z) + a_2*(2*q_z*q_w + 2*q_x*q_y) + a_3*(pow(q_x, 2) - pow(q_y, 2) - pow(q_z, 2) + pow(q_w, 2))

    return acc_to_metric(a_north), acc_to_metric(a_east), acc_to_metric(a_up)

def transform(df):
    df["a_n"], df["a_e"], df["a_u"] = zip(
        *df.apply(
            lambda row: board_to_ref(
                row["a_1"],
                row["a_2"],
                row["a_3"],
                row["x_r"],
                row["x_p"],
                row["x_y"],
            ),
            axis=1,
        )
    )

def rad_to_deg(rad):
    deg = rad*(360/(2*np.pi))

    return deg

def acc_to_metric(a):
    a_met = a/4096*9.81

    return a_met
