import numpy as np
import pandas as pd


def parse_data_file(file_path):
    try:
        with open(file_path, "r") as file:
            lines = file.readlines()
    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
        return None

    millis = []
    x_acc = []
    y_acc = []
    z_acc = []
    x_gyr = []
    y_gyr = []
    z_gyr = []
    x_mag = []
    y_mag = []
    z_mag = []

    for line in lines:
        parts = line.split()
        millis.append(int(parts[1]))
        x_acc.append(int(parts[3]))
        y_acc.append(int(parts[5]))
        z_acc.append(int(parts[7]))
        x_gyr.append(int(parts[9]))
        y_gyr.append(int(parts[11]))
        z_gyr.append(int(parts[13]))
        x_mag.append(int(parts[15]))
        y_mag.append(int(parts[17]))
        z_mag.append(int(parts[19]))

    data = {
        "sec": millis,
        "a_1": x_acc,
        "a_2": y_acc,
        "a_3": z_acc,
        "v_r": x_gyr,
        "v_p": y_gyr,
        "v_y": z_gyr,
        "m_1": x_mag,
        "m_2": y_mag,
        "m_3": z_mag,
    }

    df = pd.DataFrame(data)

    # Timestamps start at start of the board. This sets them to start at start of measurement
    start_time = df["sec"].iloc[0]
    df["sec"] = (df["sec"] - start_time) / 1000

    df['v_r'] = df['v_r']*2*np.pi/(360*(65536/4000))
    df['v_p'] = df['v_p']*2*np.pi/(360*(65536/4000))
    df['v_y'] = df['v_y']*2*np.pi/(360*(65536/4000))

    return df
