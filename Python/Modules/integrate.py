from numpy import sign
import pandas

world_cord = ['_n', '_e', '_u']
board_cord = ['_r', '_p', '_y']
    
integrals= {'a':'v',
            'v':'x',}

def integrate_wrap(df, value, system, index):
    if (index == 0): print('Integration must not start at row 0')

    result = [0,0,0]
    i = 0
    for axis in system:
        f = df[value+axis].iloc[index]
        t = df['sec'].iloc[index]
        t_prev = df['sec'].iloc[index-1]
        f_prev = df[value+axis].iloc[index-1]
        F_prev = df[(integrals[value]+axis)].iloc[index-1]
        result[i] = integral(F_prev, f, f_prev, t, t_prev)
        i += 1
    return result


def integral(F_prev, f, f_prev, t, t_prev):
    # F = F_prev + f_prev * (t - t_prev)
    F = F_prev + (f_prev + tri_sign(f, f_prev) * abs((f - f_prev) / 2)) * (t - t_prev)
    return F

def tri_sign(a, b):
    if (abs(a)>abs(b)):
        return sign(a)
    else: return sign(b)